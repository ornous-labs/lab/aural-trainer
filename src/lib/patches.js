import Tone from 'tone'

export const kick = new Tone.MembraneSynth({
  "envelope" : {
    "sustain" : 0,
    "attack" : 0.02,
    "decay" : 0.8,
  },
  "octaves" : 3,
})

export const snare =  new Tone.NoiseSynth({
  "volume" : -5,
  "envelope" : {
    "attack" : 0.001,
    "decay" : 0.2,
    "sustain" : 0
  },
  "filterEnvelope" : {
    "attack" : 0.001,
    "decay" : 0.1,
    "sustain" : 0,
  },
})

export const piano = new Tone.PolySynth(4, Tone.Synth, {
  "volume" : -8,
  "oscillator" : {
    "partials" : [1, 2, 1],
  },
  "portamento" : 0.05,
}).toMaster()

export const bass = new Tone.MonoSynth({
  "volume": -10,
  "envelope": {
    "attack": 0.1,
    "decay": 0.3,
    "release": 2,
  },
  "filterEnvelope" : {
    "attack" : 0.001,
    "decay" : 0.01,
    "sustain" : 0.5,
    "baseFrequency" : 200,
    "octaves" : 2.6,
  },
})

export const bell = new Tone.MetalSynth({
  "volume" : -15,
  "harmonicity" : 11,
  "resonance" : 800,
  "modulationIndex" : 20,
  "envelope" : {
    "decay" : 0.4,
  },
})

export const conga = new Tone.MembraneSynth({
  "volume" : -10,
  "pitchDecay" : 0.008,
  "octaves" : 2,
  "envelope" : {
    "attack" : 0.0006,
    "decay" : 0.5,
    "sustain" : 0,
  },
})
