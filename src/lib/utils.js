import Tone from 'tone'
import { piano } from './patches'

export const playInterval = (pitch, interval, scale) => {
  const offset = scale.slice(0, interval - 1).reduce((acc, val) => acc + val, 0)

  const root = Tone.Frequency(pitch)
  Tone.Transport.scheduleOnce((time) => {
    console.log('scheduling', time)
    piano.triggerAttackRelease(root, "8n", '0:1:2')
    piano.triggerAttackRelease(root.transpose(offset), "8n", '0:2:2')
    //do something with the time
  }, "0:1:0")
}

export const playRandomInterval = (level, scale) => {
  const octaveRange = [3, 4, 5]
  const pitches = ['A', 'B', 'C', 'D', 'E', 'F', 'G']

  const randomInterval = level.notes[Math.floor(Math.random() * level.notes.length)]
  const randomPitch = pitches[Math.floor(Math.random() * pitches.length)]
  const randomOctave = octaveRange[Math.floor(Math.random() * octaveRange.length)]

  playInterval(randomPitch + randomOctave, randomInterval, scale)
}

export const pickRandomInterval = (level, scale) => {
  const octaveRange = [3, 4, 5]
  const pitches = ['A', 'B', 'C', 'D', 'E', 'F', 'G']

  const interval = level.notes[Math.floor(Math.random() * level.notes.length)]
  const pitch = pitches[Math.floor(Math.random() * pitches.length)]
  const octave = octaveRange[Math.floor(Math.random() * octaveRange.length)]

  const root = Tone.Frequency(pitch + octave)
  if (interval === 0) {
    return [root, root, interval]
  }

  return [
    root,
    root.transpose(scale.slice(0, interval - 1).reduce((acc, val) => acc + val, 0)),
    interval,
  ]
}
