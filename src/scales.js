export const major = {
  modes: {
    ionian: [2, 2, 1, 2, 2, 2, 1], // Bright, happy: C-F-G
    dorian: [2, 1, 2, 2, 2, 1, 2], // Dark, yet sweet: Dm6-F
    phrygian: [1, 2, 2, 2, 1, 2, 2], // Dark, exotic: Em-F
    lydian: [2, 2, 2, 1, 2, 2, 1], // Bright, mysterious: Fmaj7#11-C
    mixolydian: [2, 2, 1, 2, 2, 1, 2], // Bright with a dark edge: G-F
    aeolian: [2, 1, 2, 2, 1, 2, 2], // Dark, sad: Am-G-F
    locrian: [1, 2, 1, 1, 2, 2, 2], // Discordant, unresolved: Bm7b5-Em
  },
}
