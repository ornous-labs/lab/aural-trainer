
// TODO Make this more sane
const levels = []

levels.push({
  name: 1,
  qualifies: (attempts, successes) => true,
  notes: [0, 5].sort(),
})

levels.push({
  name: 2,
  qualifies: (attempts, successes) => (attempts >= 10 && successes / attempts >= .8),
  notes: [...levels[0].notes, 4].sort(),
})

levels.push({
  name: 3,
  qualifies: (attempts, successes) => attempts >= 20 && sucessess / attempts >= .9,
  notes: [...levels[1].notes, 3, 2].sort(),
})

levels.push({
  name: 4,
  qualifies: (attempts, successes) => attempts >= 30 && successes / attempts >= .9,
  notes: [...levels[2].notes, 7, 8].sort(),
})

export default levels
