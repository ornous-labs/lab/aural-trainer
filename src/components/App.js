import React from 'react'
import AuralTrainer from './AuralTrainer'

const App = () => (
  <div className="App">
    <h1 className="App-Title">Hello Parcel x React</h1>
    <AuralTrainer />
  </div>
)

export default App
