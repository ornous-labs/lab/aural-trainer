import React, { useState, useEffect } from 'react'
import Tone from 'tone'
import styled from '@emotion/styled'

import levels from '../levels'
import { major } from '../scales'
import { piano } from '../lib/patches'
import { playRandomInterval, pickRandomInterval  } from '../lib/utils'

const Result = styled.span`
  color: ${({ good }) => good ? "green" : "gray"};
  padding: 0 4px;
`

const Button = styled.a`
  font-size: 1rem;
  padding: .5em 1.2em;
  background-color: rebeccapurple;
  font-family: georgia;
  font-weight: 500;
  color: white;
  border-radius: 0.15em;
  box-shadow: 1px 1px 1px white;
`

const AuralTrainer = () => {
  const [level, setLevel] = useState(0)
  const [attempts, setAttempts] = useState(0)
  const [successes, setSuccesses] = useState(0)
  const [lastResult, setLastResult] = useState(null)
  const [lastInterval, setLastInterval] = useState(null)

  useEffect(() => {
    Tone.Master.volume.value = 30
    Tone.Transport.bpm.value = 80
    Tone.Transport.start("+0.1")
  })

  const playInterval = () => {
    const [
      root,
      dest,
      lastInterval
    ] = pickRandomInterval(levels[level], major.modes.ionian)
    piano.triggerAttackRelease(root, "8n", '+0.1')
    piano.triggerAttackRelease(dest, "8n", '+1.1')
    setLastInterval(lastInterval)
  }

  const checkResult = (event) => {
    setAttempts(attempts + 1)
    const result = (lastInterval === parseInt(event.target.value, 10))
    if (result) {
      setSuccesses(successes + 1)
      if (levels.length > level + 1
        && levels[level + 1].qualifies(attempts + 1, successes + 1)) {
        setLevel(level + 1)
        alert("Congrats, you've leveled up!")
      }
    }
    setLastResult(result)
    setLastInterval(null)
    playInterval()
  }

  return (
    <React.Fragment>
      <h2>Aural Trainer</h2>
      <p>Level {level + 1} / {levels.length}</p>

      <Button onClick={playInterval.bind(this)}>Play an Interval</Button>
      {lastResult === true && <Result good>Good!</Result>}
      {lastResult === false && <Result>Bad!</Result>}
      <br />
      {lastInterval !== null && levels[level].notes.sort().map(note => (
        <button key={note} value={note} onClick={checkResult.bind(this)}>{note}</button>
      ))}
      <br />
      {lastResult !== null && (
        <p>Score: {successes}/{attempts} ({Math.floor(successes / attempts * 100)}%)</p>
      )}
    </React.Fragment>
  )
}

export default AuralTrainer
